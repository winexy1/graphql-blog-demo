const fetch = require('node-fetch');

const { CI_COMMIT_REF_NAME, CI_PROJECT_ID, FIX_SOURCE_BRANCH, PAT } =
  process.env;

const baseURL = 'https://gitlab.com/api/v4/';
const endpoint = `projects/${CI_PROJECT_ID}/merge_requests`;

async function run() {
  try {
    const response = await fetch(baseURL + endpoint, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${PAT}`,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        id: CI_PROJECT_ID,
        source_branch: FIX_SOURCE_BRANCH,
        target_branch: CI_COMMIT_REF_NAME,
        title: `fix-formatting for ${CI_COMMIT_REF_NAME}`,
      }),
    });

    const json = await response.json();

    console.log(json);
    process.exit(0);
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
}

run();
