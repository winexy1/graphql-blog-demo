const { DATABASE_URL, BACKEND_VERSION } = process.env;

const app = {
  name: 'graphql-app',
  script: 'npm',
  args: 'start',
  env: {
    DATABASE_URL,
    BACKEND_VERSION,
  },
};

const output = `
  module.exports = {
    apps: [${JSON.stringify(app, null, 2)}]
  }
`;

console.log(output);
